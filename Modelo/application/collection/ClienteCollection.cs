﻿using Modelo.application.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.application.collection
{
    public class ClienteCollection
    {
        //Atributo
        private List<Cliente> clientes;
        //Constructor sin parametros
        public ClienteCollection()
        {
            clientes = new List<Cliente>();
        }
        //Constructor con parametros
        public ClienteCollection(List<Cliente> clientes)
        {
            this.Clientes = clientes;
        }
        //Accesadores y Mutadores
        public List<Cliente> Clientes { get => clientes; set => clientes = value; }

        public void Agregar(Cliente cliente) {

            if (!ExisteCliente(cliente.Rut))
            {
                clientes.Add(cliente);
            }
            else {
                throw new Exception("El cliente ya existe.");
            }

        }

        public bool ExisteCliente(string rut) {
            bool existe = false;

            foreach (Cliente cli in clientes)
            {
                if (cli.Rut.Equals(rut))
                {
                    existe = true;
                }
            }

            return existe;
        }

        public void Eliminar(Cliente cliente) {

            if (cliente != null)
            {
                Eliminar(cliente.Rut);
            }
            else {
                throw new Exception("Elemento seleccionado no es válido.");
            }
        }

        public void Eliminar(string rut) {

            int indice = -1;

            for (int i = 0; i < clientes.Count; i++)
            {
                if (clientes[i].Rut == rut) {

                    indice = i;
                }
            }

            if (indice == -1)
            {
                throw new Exception("El cliente no existe en el sistema.");
            }
            else {
                clientes.RemoveAt(indice);
            }
        }

        public void Actualizar(Cliente cliente) {

            int indice = -1;

            for (int i = 0; i < clientes.Count; i++)
            {
                if (clientes[i].Rut == cliente.Rut)
                {

                    indice = i;
                }
            }

            if (indice == -1)
            {
                throw new Exception("El cliente no existe en el sistema.");
            }
            else
            {
                clientes[indice].RazonSocial = cliente.RazonSocial;
                clientes[indice].NombreContacto = cliente.NombreContacto;
                clientes[indice].MailContacto = cliente.MailContacto;
                clientes[indice].Direccion = cliente.Direccion;
                clientes[indice].Telefono = cliente.Telefono;
                clientes[indice].Actividad = cliente.Actividad;
                clientes[indice].Tipo = cliente.Tipo;
            }
        }

        public Cliente Buscar(string rut) {

            Cliente cliente = new Cliente();

            int indice = -1;

            for (int i = 0; i < clientes.Count; i++)
            {
                if (clientes[i].Rut == rut) {
                    indice = i;
                }
            }


            if (indice == -1)
            {
                throw new Exception("El cliente no existe en el sistema.");
            }
            else {
                cliente = clientes[indice];
            }

            return cliente;
        }

        public List<Cliente> Buscar(ActividadEmpresa actividad) {

            List<Cliente> clientesFiltroActividad = new List<Cliente>();

            for (int i = 0; i < clientes.Count; i++)
            {
                if (actividad.Id.Equals(clientes[i].Actividad.Id)) {

                    clientesFiltroActividad.Add(clientes[i]);
                }
            }

            if (clientesFiltroActividad.Count == 0)
            {
                throw new Exception("No se encontraron clientes con la actividad " 
                    + actividad.Descripcion);
            }

            return clientesFiltroActividad;

        }

        public List<Cliente> Buscar(TipoEmpresa tipo)
        {

            List<Cliente> clientesFiltroTipo = new List<Cliente>();

            for (int i = 0; i < clientes.Count; i++)
            {
                if (tipo.Id.Equals(clientes[i].Tipo.Id))
                {

                    clientesFiltroTipo.Add(clientes[i]);
                }
            }

            if (clientesFiltroTipo.Count == 0) {
                throw new Exception("No se encontraron clientes con el tipo "
                    + tipo.Descripcion);
            }

            return clientesFiltroTipo;

        }

    }
}
