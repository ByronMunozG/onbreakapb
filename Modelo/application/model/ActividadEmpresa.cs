﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.application.model
{
    public class ActividadEmpresa
    {
        //Atributos
        private int id;
        private string descripcion;
        //Constructor sin parametros
        public ActividadEmpresa()
        {

        }
        //Constructor con parametros
        public ActividadEmpresa(int id, string descripcion)
        {
            this.Id = id;
            this.Descripcion = descripcion;
        }
        //Accesadores y Mutadores
        public int Id { get => id; set => id = value; }
        public string Descripcion { get => descripcion; set => descripcion = value; }

        public override string ToString()
        {
            return descripcion;
        }
    }
}
