﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.application.model
{
    public class Cliente
    {
        //Atributos
        private string rut;
        private string razonSocial;
        private string nombreContacto;
        private string mailContacto;
        private string telefono;
        private string direccion;
        private ActividadEmpresa actividad;
        private TipoEmpresa tipo;

        public Cliente()
        {

        }
        //Constructor con parametros
        public Cliente(string rut, string razonSocial, string nombreContacto, string mailContacto, string telefono, string direccion, ActividadEmpresa actividad, TipoEmpresa tipo)
        {
            this.Rut = rut;
            this.RazonSocial = razonSocial;
            this.NombreContacto = nombreContacto;
            this.MailContacto = mailContacto;
            this.Telefono = telefono;
            this.Direccion = direccion;
            this.Actividad = actividad;
            this.Tipo = tipo;
        }
        //Accesadores y Mutadores
        public string Rut { get => rut; set => rut = value; }
        public string RazonSocial { get => razonSocial; set => razonSocial = value; }
        public string NombreContacto { get => nombreContacto; set => nombreContacto = value; }
        public string MailContacto { get => mailContacto; set => mailContacto = value; }
        public string Telefono { get => telefono; set => telefono = value; }
        public string Direccion { get => direccion; set => direccion = value; }
        public ActividadEmpresa Actividad { get => actividad; set => actividad = value; }
        public TipoEmpresa Tipo { get => tipo; set => tipo = value; }

        public override string ToString()
        {
            return rut + " - " + razonSocial;
        }
    }
}
