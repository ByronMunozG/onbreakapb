﻿using Modelo.application.collection;
using Modelo.application.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Presentacion.application.controller
{
    /// <summary>
    /// Lógica de interacción para MantenedorClientes.xaml
    /// </summary>
    public partial class MantenedorClientes : Window
    {

        private ClienteCollection coleccion;
        public MantenedorClientes()
        {
            InitializeComponent();
            IniciarActividadEmpresa();
            IniciarTipoEmpresa();
            DesactivarBotonActualizar();
        }

        public MantenedorClientes(ClienteCollection coleccion)
        {
            InitializeComponent();
            IniciarActividadEmpresa();
            IniciarTipoEmpresa();
            this.coleccion = coleccion;
            ActualizarDataGrid();
            DesactivarBotonActualizar();
        }

        public void DesactivarBotonActualizar()
        {
            btn_actualizar.IsEnabled = false;
            btn_modificar.IsEnabled = true;
            btn_guardar.IsEnabled = true;
            txt_rut.IsEnabled = true;
            btn_borrar.IsEnabled = true;
            btn_limpiar.IsEnabled = true;
        }

        public void ActivarBotonActualizar() {

            btn_actualizar.IsEnabled = true;
            btn_modificar.IsEnabled = false;
            btn_guardar.IsEnabled = false;
            txt_rut.IsEnabled = false;
            btn_borrar.IsEnabled = false;
            btn_limpiar.IsEnabled = false;
        }

        public void IniciarActividadEmpresa() {
            //Creación lista actividades
            List<ActividadEmpresa> actividades = new List<ActividadEmpresa>();
            actividades.Add(new ActividadEmpresa(10, "Agropecuaria"));
            actividades.Add(new ActividadEmpresa(20, "Minería"));
            actividades.Add(new ActividadEmpresa(30, "Manofactura"));
            actividades.Add(new ActividadEmpresa(40, "Comercio"));
            actividades.Add(new ActividadEmpresa(50, "Hoteleria"));
            actividades.Add(new ActividadEmpresa(60, "Alimentos"));
            actividades.Add(new ActividadEmpresa(70, "Transporte"));
            actividades.Add(new ActividadEmpresa(80, "Servicios"));
            //Se agrega lista a combobox
            cmb_actividad.ItemsSource = actividades;
            //Se refresca o renderiza combobox
            cmb_actividad.Items.Refresh();
        }

        public void IniciarTipoEmpresa() {
            //Creación lista tipo
            List<TipoEmpresa> tipos = new List<TipoEmpresa>();
            tipos.Add(new TipoEmpresa(1, "SPA"));
            tipos.Add(new TipoEmpresa(2, "EIRL"));
            tipos.Add(new TipoEmpresa(3, "Limitada"));
            tipos.Add(new TipoEmpresa(4, "Sociedad Anónima"));
            //Se agrega lista a combobox
            cmb_tipo.ItemsSource = tipos;
            //Se refresca o renderiza combobox
            cmb_tipo.Items.Refresh();
        }

        private void Btn_guardar_Click(object sender, RoutedEventArgs e)
        {
            string rut = txt_rut.Text;
            string razonSocial = txt_razon_social.Text;
            string nombreContacto = txt_nombre_contacto.Text;
            string mailContacto = txt_mail_contacto.Text;
            string telefono = txt_telefono.Text;
            string direccion = txt_direccion.Text;
            ActividadEmpresa actividad = (ActividadEmpresa)cmb_actividad.SelectedItem;
            TipoEmpresa tipo = (TipoEmpresa)cmb_tipo.SelectedItem;

            Cliente cliente = new Cliente(rut, razonSocial, nombreContacto,
                mailContacto, telefono, direccion, actividad, tipo);
            try
            {
                this.coleccion.Agregar(cliente);
                Limpiar();
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
            ActualizarDataGrid();
        }

        private void ActualizarDataGrid() {

            dtg_clientes.ItemsSource = coleccion.Clientes;
            dtg_clientes.Items.Refresh();
        }

        private void Btn_borrar_Click(object sender, RoutedEventArgs e)
        {
            object filaSeleccionada = dtg_clientes.SelectedItem;

            if (filaSeleccionada != null) {

                if(filaSeleccionada.GetType() == typeof(Cliente)) {

                    Cliente cliente = (Cliente)filaSeleccionada;
                    try
                    {
                        coleccion.Eliminar(cliente);
                        ActualizarDataGrid();
                        MessageBox.Show("El Cliente " + cliente.RazonSocial + " fue eliminado del sistema.");
                    }
                    catch (Exception ex) {
                        MessageBox.Show(ex.Message);
                    }
                }
                else {
                    MessageBox.Show("Elemento seleccionado no es válido.");
                }
            }
            else {

                MessageBox.Show("Debe seleccionar una fila de la tabla.");
            }
        }

        private void Btn_modificar_Click(object sender, RoutedEventArgs e)
        {
            object filaSeleccionada = dtg_clientes.SelectedItem;

            if (filaSeleccionada != null)
            {
                if (filaSeleccionada.GetType() == typeof(Cliente))
                {

                    Cliente cliente = (Cliente)filaSeleccionada;
                    try
                    {
                        CargarDatosFormulario(cliente);
                        ActivarBotonActualizar();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
                else
                {
                    MessageBox.Show("Elemento seleccionado no es válido.");
                }
            }
            else
            {

                MessageBox.Show("Debe seleccionar una fila de la tabla.");
            }
        }

        private void CargarDatosFormulario(Cliente cliente) {

            txt_rut.Text = cliente.Rut;
            txt_razon_social.Text = cliente.RazonSocial;
            txt_nombre_contacto.Text = cliente.NombreContacto;
            txt_mail_contacto.Text = cliente.MailContacto;
            txt_direccion.Text = cliente.Direccion;
            txt_telefono.Text = cliente.Telefono;
            cmb_actividad.SelectedValue = cliente.Actividad;
            cmb_tipo.SelectedValue = cliente.Tipo;
        }

        public void Limpiar() {

            txt_rut.Text = "";
            txt_razon_social.Text = "";
            txt_nombre_contacto.Text = "";
            txt_mail_contacto.Text = "";
            txt_direccion.Text = "";
            txt_telefono.Text = "";
            cmb_actividad.SelectedValue = null;
            cmb_tipo.SelectedValue = null;
        }

        private void Btn_actualizar_Click(object sender, RoutedEventArgs e)
        {
            string rut = txt_rut.Text;
            string razonSocial = txt_razon_social.Text;
            string nombreContacto = txt_nombre_contacto.Text;
            string mailContacto = txt_mail_contacto.Text;
            string telefono = txt_telefono.Text;
            string direccion = txt_direccion.Text;
            ActividadEmpresa actividad = (ActividadEmpresa)cmb_actividad.SelectedItem;
            TipoEmpresa tipo = (TipoEmpresa)cmb_tipo.SelectedItem;

            Cliente cliente = new Cliente(rut, razonSocial, nombreContacto,
                mailContacto, telefono, direccion, actividad, tipo);
            try
            {
                this.coleccion.Actualizar(cliente);
                DesactivarBotonActualizar();
                Limpiar();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            ActualizarDataGrid();
        }

        private void Btn_limpiar_Click(object sender, RoutedEventArgs e)
        {
            Limpiar();
        }
    }
}
