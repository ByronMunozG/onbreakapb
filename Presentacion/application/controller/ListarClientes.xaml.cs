﻿using Modelo.application.collection;
using Modelo.application.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Presentacion.application.controller
{
    /// <summary>
    /// Lógica de interacción para ListarClientes.xaml
    /// </summary>
    public partial class ListarClientes : Window
    {
        private ClienteCollection coleccion;
        public ListarClientes()
        {
            InitializeComponent();
            ActivarRadioButtonRut();
            IniciarActividadEmpresa();
            IniciarTipoEmpresa();
        }


        public ListarClientes(ClienteCollection coleccion)
        {
            InitializeComponent();
            this.coleccion = coleccion;
            ActivarRadioButtonRut();
            ActualizarDataGrid();
            IniciarActividadEmpresa();
            IniciarTipoEmpresa();
        }

        public void IniciarActividadEmpresa()
        {
            //Creación lista actividades
            List<ActividadEmpresa> actividades = new List<ActividadEmpresa>();
            actividades.Add(new ActividadEmpresa(10, "Agropecuaria"));
            actividades.Add(new ActividadEmpresa(20, "Minería"));
            actividades.Add(new ActividadEmpresa(30, "Manofactura"));
            actividades.Add(new ActividadEmpresa(40, "Comercio"));
            actividades.Add(new ActividadEmpresa(50, "Hoteleria"));
            actividades.Add(new ActividadEmpresa(60, "Alimentos"));
            actividades.Add(new ActividadEmpresa(70, "Transporte"));
            actividades.Add(new ActividadEmpresa(80, "Servicios"));
            //Se agrega lista a combobox
            cmb_actividad.ItemsSource = actividades;
            //Se refresca o renderiza combobox
            cmb_actividad.Items.Refresh();
        }

        public void IniciarTipoEmpresa()
        {
            //Creación lista tipo
            List<TipoEmpresa> tipos = new List<TipoEmpresa>();
            tipos.Add(new TipoEmpresa(1, "SPA"));
            tipos.Add(new TipoEmpresa(2, "EIRL"));
            tipos.Add(new TipoEmpresa(3, "Limitada"));
            tipos.Add(new TipoEmpresa(4, "Sociedad Anónima"));
            //Se agrega lista a combobox
            cmb_tipo.ItemsSource = tipos;
            //Se refresca o renderiza combobox
            cmb_tipo.Items.Refresh();
        }

        public void ActivarRadioButtonRut() {
            //Activar RadioButton + TextBox
            rdb_rut.IsChecked = true;
            txt_rut.IsEnabled = true;
            //Desactivar RadioButton + ComboBox
            rdb_actividad.IsChecked = false;
            cmb_actividad.IsEnabled = false;
            rdb_tipo.IsChecked = false;
            cmb_tipo.IsEnabled = false;
        }

        public void ActivarRadioButtonActividad()
        {
            //Activar RadioButton + Combobox
            rdb_actividad.IsChecked = true;
            cmb_actividad.IsEnabled = true;
            //Desactivar RadioButton + ComboBox + TextBox
            rdb_rut.IsChecked = false;
            txt_rut.IsEnabled = false;
            rdb_tipo.IsChecked = false;
            cmb_tipo.IsEnabled = false;
        }

        public void ActivarRadioButtonTipo()
        {
            //Activar RadioButton + Combobox
            rdb_tipo.IsChecked = true;
            cmb_tipo.IsEnabled = true;
            //Desactivar RadioButton + ComboBox + TextBox
            rdb_rut.IsChecked = false;
            txt_rut.IsEnabled = false;
            rdb_actividad.IsChecked = false;
            cmb_actividad.IsEnabled = false;
        }

        private void ActualizarDataGrid()
        {
            dtg_clientes.ItemsSource = coleccion.Clientes;
            dtg_clientes.Items.Refresh();
        }

        private void Rdb_rut_Checked(object sender, RoutedEventArgs e)
        {
            ActivarRadioButtonRut();
            Limpiar();
        }

        private void Rdb_actividad_Checked(object sender, RoutedEventArgs e)
        {
            ActivarRadioButtonActividad();
            Limpiar();
        }

        private void Rdb_tipo_Checked(object sender, RoutedEventArgs e)
        {
            ActivarRadioButtonTipo();
            Limpiar();
        }

        private void Btn_buscar_cliente_Click(object sender, RoutedEventArgs e)
        {
            bool rutActivo = (rdb_rut.IsChecked != null)? (bool)rdb_rut.IsChecked:false;
            bool ActividadActivo = (rdb_actividad.IsChecked != null) ? (bool)rdb_actividad.IsChecked : false;
            bool TipoActivo = (rdb_tipo.IsChecked != null) ? (bool)rdb_tipo.IsChecked : false;

            if (rutActivo) {

                string rut = txt_rut.Text;
                if (rut != null && rut.Length > 0)
                {
                    try {
                        Cliente cliente = coleccion.Buscar(rut);
                        List<Cliente> clienteFiltradoPorRut = new List<Cliente>();
                        clienteFiltradoPorRut.Add(cliente);
                        dtg_clientes.ItemsSource = clienteFiltradoPorRut;
                        dtg_clientes.Items.Refresh();
                    }
                    catch (Exception ex) {
                        dtg_clientes.ItemsSource = new List<Cliente>();
                        dtg_clientes.Items.Refresh();
                        MessageBox.Show(ex.Message);
                    }
                }
                else {
                    ActualizarDataGrid();
                }
            }

            if (ActividadActivo) {

                object actividadSeleccionada = cmb_actividad.SelectedItem;

                if (actividadSeleccionada != null)
                {
                    ActividadEmpresa actividad = (ActividadEmpresa)actividadSeleccionada;

                    try
                    {
                        List<Cliente> filtradoPorActividad = coleccion.
                            Buscar(actividad);
                        dtg_clientes.ItemsSource = filtradoPorActividad;
                        dtg_clientes.Items.Refresh();
                    }
                    catch (Exception ex) {
                        dtg_clientes.ItemsSource = new List<Cliente>();
                        dtg_clientes.Items.Refresh();
                        MessageBox.Show(ex.Message);
                    }
                }
                else {
                    MessageBox.Show("Debe seleccionar una actividad.");
                }
            }

            if (TipoActivo) {
                object tipoSeleccionado = cmb_tipo.SelectedItem;

                if (tipoSeleccionado != null)
                {
                    TipoEmpresa tipo = (TipoEmpresa)tipoSeleccionado;

                    try
                    {
                        List<Cliente> filtradoPorTipo = coleccion.
                            Buscar(tipo);
                        dtg_clientes.ItemsSource = filtradoPorTipo;
                        dtg_clientes.Items.Refresh();
                    }
                    catch (Exception ex)
                    {
                        dtg_clientes.ItemsSource = new List<Cliente>();
                        dtg_clientes.Items.Refresh();
                        MessageBox.Show(ex.Message);
                    }
                }
                else
                {
                    MessageBox.Show("Debe seleccionar un tipo.");
                }
            }
        }

        public void Limpiar() {
            txt_rut.Text = "";
            cmb_actividad.SelectedItem = null;
            cmb_tipo.SelectedItem = null;
        }
    }
}
