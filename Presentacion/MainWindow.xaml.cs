﻿using Modelo.application.collection;
using Presentacion.application.controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Presentacion
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ClienteCollection coleccion;
        public MainWindow()
        {
            InitializeComponent();
            coleccion = new ClienteCollection();
        }

        private void Btn_mantenedor_clientes_Click(object sender, RoutedEventArgs e)
        {
            MantenedorClientes mantenedorClientes = new MantenedorClientes(coleccion);
            mantenedorClientes.Owner = this;
            mantenedorClientes.ShowDialog();
        }

        private void Btn_listar_clientes_Click(object sender, RoutedEventArgs e)
        {
            ListarClientes listarClientes = new ListarClientes(coleccion);
            listarClientes.Owner = this;
            listarClientes.ShowDialog();
        }
    }
}
